from base.page_utils import PageUtils
import time
class Post_Job_Page(PageUtils):

    popup_close=("id","image_view_close")
    hiring_tab =("xpath","// android.widget.FrameLayout[@content-desc = 'Hiring']/android.widget.ImageView")
    post_a_job_icon=("id","button_post_job")
    job_category_field=("id","edittext_job_category")
    def job_category(self, category):
      job_category="//android.widget.TextView[@text='{0}']"
      category_xp=job_category.format(category)
      job_cat=('xpath',category_xp)
      return job_cat

    def job_specialization(self, sp):
        spec="//android.widget.TextView[@text='{0}']"
        specialization=spec.format(sp)
        job_spec=("xpath",specialization)
        return job_spec
    job_title_field=("id","edittext_job_title")
    edit_n_search=("id","edit_text_search")
    def job_title(self, title1):
        self.send_keys(self.edit_n_search,title1)
        time.sleep(3)
        title_xp="//android.widget.TextView[@text='{0}']"
        job_t_xp=title_xp.format(title1)
        job_title_xp=("xpath",job_t_xp)
        return job_title_xp
    button_nxt=("xpath","//android.widget.Button[@text='NEXT']")
    skill1 = ("xpath","//android.widget.CompoundButton[@index='5']")
    skill2 = ("xpath", "//android.widget.CompoundButton[@index='1']")
    skill3 = ("xpath", "//android.widget.CompoundButton[@index='2']")
    skill4 = ("xpath", "//android.widget.CompoundButton[@index='3']")
    skill5 = ("xpath", "//android.widget.CompoundButton[@index='4']")
    publish_job=("xpath","//android.widget.Button[@index='1' and @text='PUBLISH JOB']")
    diff_cat=("id","button_yes")

    def close_popup(self):

           if self.is_visible(self.popup_close) is True:
                                        self.click(self.popup_close)
           else:
                print("no popup")
    def diff_cat_detected(self):
        if self.is_visible(self.diff_cat) is True:
            self.click(self.diff_cat)
        else:
            print("same category")

    def click_post_job_button(self, category,sp, title1):
        self.close_popup()
        self.click(self.hiring_tab)
        self.click( self.post_a_job_icon)
        self.click(self.job_category_field)
        self.click(self.job_category(category))
        self.click(self.job_specialization(sp))
        self.click(self.job_title_field)
        self.click(self.job_title(title1))
        time.sleep(2)
        self.diff_cat_detected()
        self.click(self.button_nxt)
        time.sleep(4)
        self.click(self.button_nxt)
        self.click(self.button_nxt)
        self.click(self.skill1)
        self.click(self.skill2)
        self.click(self.skill3)
        self.click(self.skill4)
        self.click(self.skill5)
        self.click(self.button_nxt)
        self.click(self.button_nxt)
        self.click(self.button_nxt)

        self.click(self.publish_job)
        self.take_screenshot()






