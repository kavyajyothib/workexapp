from base.page_utils import PageUtils
import time
class LoginAsBO(PageUtils):



    mobile_no_field=("id","edit_text_number")
    login_button=("xpath","//android.widget.Button[@id='button_next' and contains(text,'Login')]")
    otp=("class","android.widget.EditText")
    login_as_business=("id","button_bussiness")
    location_permission_allow=("id","permission_allow_foreground_only_button")
    location_of_bo=("id","text_input_edit_text_company_location")
    current_location_select=("id","textview_current_location")

    def enter_mobile_no_for_login(self,data):
        self.send_keys(self.mobile_no_field, data)
    def click_login(self):
        self.click(self.login_button)
    def enter_otp(self, data):
        self.send_keys(self.otp, data)
    def click_login_as_bo(self):
        self.click(self.login_as_business)
    def click_location_field(self):
        self.click(self.location_of_bo)
    def click_current_location(self):
        self.click(self.current_location_select)

