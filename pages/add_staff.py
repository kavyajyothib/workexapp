from base.page_utils import PageUtils
class AddStaff(PageUtils):
    staff_tab=("xpath","//android.widget.TextView[@text='Staff']")
    all_staff_tab=("xpath","//android.widget.LinearLayout[@content-desc='All Staff']")
    add_staff_button=("id","button_add_staff")
    employee_full_name=("id","edit_text_name")
    employee_mobile_number=("id","edit_text_number")
    staff_designation=("id","edit_text_designation")
    select_designation=("xpath","//android.widget.TextView[@index='0']")
    joining_date=("id","edit_text_join_date")
    calander_ok=("id",'confirm_button')

    nxt_button=("id","button_next")
    salary=("id","edit_text_salary")
    manage_from_date=("id","edit_text_manage_date")


    def dateselection(self,date):
        date_selection ="//android.widget.TextView[@content-desc='{0}']"
        date_sent = date_selection.format(date)
        date_xp=("xpath",date_sent)
        return date_xp
    def select_gender(self,gender):
        gender_d = "//androidx.recyclerview.widget.RecyclerView[2]/android.widget.CheckBox[@text='{0}']"
        gender_data=gender_d.format(gender)
        gender_xp=("xpath", gender_data)
        return gender_xp
    def select_payroll_type(self, payroll):
        payroll_t="//android.widget.TextView[@text='{0}']"
        payroll_type=payroll_t.format(payroll)
        payroll_xp=("xpath",payroll_type)
        return payroll_xp

    def click_staff_tab(self):
        self.click(self.staff_tab)
    def click_all_staff(self):
        self.click(self.all_staff_tab)
    def click_add_staff_button(self):
        self.click(self.add_staff_button)
    def enter_employee_name(self, name):
        self.send_keys(self.employee_full_name,name)
    def enter_employee_number(self, mobile_no):
        self.send_keys(self.employee_mobile_number, mobile_no)
    def enter_staff_designstion(self, designation):
        self.send_keys(self.staff_designation, designation)
    def click_joining_date(self,x):
        self.click(self.joining_date)
        self.click(self.dateselection(x))
        self.click(self.calander_ok)
    def click_gender(self, gender):
        self.click(self.select_gender(gender))

    def click_nxt(self):
        self.click(self.nxt_button)
    def click_payroll_type(self, payroll):
        self.click(self.select_payroll_type(payroll))
    def send_salary(self, salary):
        self.send_keys(self.salary, salary)
    def click_manage_from_date(self):
        self.click(self.manage_from_date)

        self.click(self.calander_ok)

    def go_to_add_staff(self):
        self.click_staff_tab()
        self.click_all_staff()
        self.click_add_staff_button()

    def add_staff(self, name, mobile_no, designation,date, gender, payroll):
        self.enter_employee_name(name)
        self.enter_employee_number(mobile_no)
        self.enter_staff_designstion(designation)
        self.scroll_app()
        self.click_joining_date(date)
        self.click_gender(gender)
        self.click_nxt()
        self.click_payroll_type(payroll)
        self.click_nxt()

    def add_staff_details(self, salary):
        self.send_salary(salary)
        self.click_nxt()
        self.click_manage_from_date()
        self.click_nxt()


