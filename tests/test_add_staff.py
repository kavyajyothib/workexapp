from appium import webdriver
from tests.base_test  import BaseTests
# from settings import TEST_DATA


class Test_AddStaff(BaseTests):
    def test_daily_staff_addition(self,init):
        self.asp.go_to_add_staff()
        self.asp.add_staff( name="kavya", mobile_no="8877441122",designation="QA Engineer", date="Wed, Jun 2", gender="Female", payroll="Daily")
        self.asp.add_staff_details(salary="2000")