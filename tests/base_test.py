import pytest
from pages.post_job_page import Post_Job_Page
from pages.add_staff import AddStaff
from pages.login import  LoginAsBO
# from actions.login_actions import LoginActions


@pytest.mark.usefixtures('setup')
class BaseTests:

    @pytest.fixture
    def init(self):
        driver = self.driver
        self.lbo = LoginAsBO(driver)
        self.pjp = Post_Job_Page(driver)
        self.asp= AddStaff(driver)

        # self.login_actions = LoginActions(driver)
