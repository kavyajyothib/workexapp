from appium import webdriver
import pytest
@pytest.fixture()
def setup(request):
    capabilities = {
        "platformName": "Android",
        "platformVersion": "10.0",
        "deviceName": "android",
        "app": "/home/kavya/Desktop/workex1.apk",
        "appPackage": "com.workexjobapp",
        "appActivity": "com.workexjobapp.ui.activities.splash.SplashActivity",
        "noReset": True
    }
    url = 'http://localhost:4723/wd/hub'
    request.instance.driver = webdriver.Remote(url, capabilities)
def teardown(request):
    request.instance.driver.quit()
    request.addfinalizer(teardown)

